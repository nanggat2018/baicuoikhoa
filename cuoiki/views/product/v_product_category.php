<!-- start sub-menu Area -->
<div class="sub-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--                <div class="sub-menu">-->
                <!--                    <nav class="sub-menu-list">-->
                <!--                        <ul class="item">-->
                <!--                            <li><a href="#">Home<span class="shape-right"></span></a></li>-->
                <!--                            <li><a href="#">Products<span class="shape-right"></span></a></li>-->
                <!--                            <li><a href="#">Santa Fe Jacket For Men</a></li>-->
                <!--                        </ul>-->
                <!--                    </nav>-->
                <!--                </div>-->
            </div>
        </div>
    </div>
</div>
<!-- End sub-menu Area -->
<!-- Start product banner Area -->
<div class="product-banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pro-banner">
                    <!--                    <img src="public/layout/assets/images/banner/pro-ban-1.webp" alt="" />-->
                    <img style="height: 400px" src="admin/public/image_san_pham/bia1.png">
                    <a class="pro-btn-1" href="#">women clothing</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End product banner Area -->
<!-- Start product list Area -->
<div class="product-list-content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-by">
                    <h4>sản phẩm</h4>
                </div>
                <div class="pro-list-left lone">
                    <h5>theo danh mục</h5>
                </div>
                <!-- product category list -->
                <ul class="pro-cata-name">
                    <?php foreach ($product_category as $key=>$value){ ?>
                        <li><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='product_category.php?id=<?php echo $value->id;?>'"><?php echo $value->ten_danh_muc; ?></button></li>
                        <!--                    <li><a href="#">Túi sách Việt Nam</a></li>-->
                        <!--                    <li><a href="#">Túi du lịch Nhật Bản</a></li>-->
                        <!--                    <li><a href="#">Túi da đeo vai Thái Lan</a></li>-->
                    <?php }?>
                </ul>
                <!-- product price -->
                <div class="by-price">
                    <div class="pro-list-left">
                        <h5>giá tiền</h5>
                    </div>
                    <p>

                        <label for="amount" style="font-size: 12px; color: #e17009 ">Phạm vi giá: 1,000,000đ - 10,000,000đ</label>
                        <!--                        <input type="text" id="amount" readonly-->
                        <!--                               style="border:0; color:#f6931f; font-weight:bold;">-->
                    </p>
                    <div id="slider-range"></div>
                </div>
                <!-- product by color -->
<!--                <div class="pro-list-left ltwo">-->
<!--                    <h5>màu sắc</h5>-->
<!--                </div>-->
<!--                <ul class="color">-->
<!--                    <li><a class="color-1" href="#"></a></li>-->
<!--                    <li><a class="color-2" href="#"></a></li>-->
<!--                    <li><a class="color-3" href="#"></a></li>-->
<!--                    <li><a class="color-4" href="#"></a></li>-->
<!--                    <li><a class="color-5" href="#"></a></li>-->
<!--                    <li><a class="color-6" href="#"></a></li>-->
<!--                </ul>-->
<!--                <div class="pro-list-left">-->
<!--                    <h5>kích cỡ</h5>-->
<!--                </div>-->
<!--                <ul class="size">-->
<!--                    <li><a href="#">XS</a></li>-->
<!--                    <li><a href="#">S</a></li>-->
<!--                    <li><a href="#">M</a></li>-->
<!--                    <li><a href="#">L</a></li>-->
<!--                    <li><a href="#">XL</a></li>-->
<!--                </ul>-->
                <!--                <div class="pro-list-left lone">-->
                <!--                    <h5>product tags</h5>-->
                <!--                </div>-->
                <!--                <ul class="pro-tags">-->
                <!--                    <li><a href="#">Clothing</a></li>-->
                <!--                    <li><a href="#">Bags</a></li>-->
                <!--                    <li><a href="#">Women</a></li>-->
                <!--                    <li><a href="#">Tie</a></li>-->
                <!--                    <li><a href="#">Men</a></li>-->
                <!--                    <li><a href="#">dress</a></li>-->
                <!--                </ul>-->
                <!--                <div class="pro-list-left lone">-->
                <!--                    <h5>compare</h5>-->
                <!--                </div>-->
                <!--                <div class="compare-btm">-->
                <!--                    <a class="compare-ico" href="#"><i class="pe-7s-search"></i></a>-->
                <!--                    <a class="compare" href="#">-->
                <!--                        White men’s polo-->
                <!--                    </a>-->
                <!--                </div>-->
                <!--                <div class="compare-btm">-->
                <!--                    <a class="compare-ico" href="#"><i class="pe-7s-search"></i></a>-->
                <!--                    <a class="compare" href="#">-->
                <!--                        T-shirt for style girl...-->
                <!--                    </a>-->
                <!--                </div>-->
                <!--                <div class="compare-btm">-->
                <!--                    <a class="compare-ico" href="#"><i class="pe-7s-search"></i></a>-->
                <!--                    <a class="compare" href="#">-->
                <!--                        Basic dress for women...-->
                <!--                    </a>-->
                <!--                </div>-->
                <div class="arival pro-list">
                    <h4 style="font-size: 15px">sản phẩm bán chạy</h4>
                </div>
                <!-- Single Right banner total owl -->
                <div class="banner-right-area owl-nav-syle-1">
                    <!-- Single Right banner group-->
                    <div class="banner-right-group">
                        <?php foreach ($bag_products as $key=>$value) {?>
                            <div class="modi-all">
                                <div class="banner-right-img">
                                    <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" /></a>
                                </div>
                                <div class="banner-right-content">
                                    <a href="#"><?php echo $value->ten_san_pham ?></a>
                                    <h2><?php echo number_format($value->gia_tien)?>₫</h2>
                                    <div class="pro-star banner">
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star-change"><i class="fa fa-star"></i></span>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                    <!-- Single Right banner group-->
                    <div class="banner-right-group">

                        <?php foreach ($wallet_products as $key=>$value){ ?>
                            <div class="modi-all">
                                <div class="banner-right-img">
                                    <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh?>" alt="" /></a>
                                </div>
                                <div class="banner-right-content">
                                    <a href="#"><?php echo $value->ten_san_pham ?></a>
                                    <h2><?php echo number_format($value->gia_tien) ?>₫</h2>
                                    <div class="pro-star banner">
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star"><i class="fa fa-star"></i></span>
                                        <span class="star-change"><i class="fa fa-star"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Right banner-->
                        <?php } ?>

                    </div>
                </div>
            </div>
            <!--start  product list middle top-->
            <div class="col-lg-9">
                <div class="product-navbar d-flex justify-content-between flex-wrap">
                    <!--                    <ul class="nav navbar-dropdown">-->
                    <!--                        <li class="dropdown">-->
                    <!--                            <a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Default softing-->
                    <!--                                <span class="caret"></span></a>-->
                    <!--                            <ul class="dropdown-menu">-->
                    <!--                                <li><a href="#">Page 1-1</a></li>-->
                    <!--                                <li><a href="#">Page 1-2</a></li>-->
                    <!--                                <li><a href="#">Page 1-3</a></li>-->
                    <!--                            </ul>-->
                    <!--                        </li>-->
                    <!--                        <li class="dropdown">-->
                    <!--                            <a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Show by <span class="caret"></span></a>-->
                    <!--                            <ul class="dropdown-menu">-->
                    <!--                                <li><a href="#">Page 1-1</a></li>-->
                    <!--                                <li><a href="#">Page 1-2</a></li>-->
                    <!--                                <li><a href="#">Page 1-3</a></li>-->
                    <!--                            </ul>-->
                    <!--                        </li>-->
                    <!--                    </ul>-->
                    <div class="product-show">
                        <p style="margin-left: 50px;" >Hiển thị 1-14 trong số 50 kết quả</p>
                    </div>
                    <div class="navbar-icon">
                        <!-- Nav tabs -->
                        <ul class="nav">
                            <li><a class="active" href="#grid" data-bs-toggle="tab"><i class="fa fa-th"></i></a></li>
                            <li><a href="#list" data-bs-toggle="tab"><i class="fa fa-th-list"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!--End  product list middle top-->

                <!--total pro list content start-->
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="grid">
                        <div class="product-grid-wrapper">
                            <div class="row">
                                <?php foreach ($product_category_detail as $key=>$value) {?>
                                    <div class="col-md-4 col-sm-6">
                                        <!--start single-project -->
                                        <div class="single-product">
                                            <div class="single-product-top">
                                                <a href="#">
                                                    <img class="pro-font-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" />
                                                    <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh_hover;?>" alt="" />
                                                </a>
                                                <!--                                            <h3 class="pro-price">$95</h3>-->
                                                <h6 ><a href="shop-details.html"><?php echo $value->ten_san_pham;?></a></h6>
                                                <h6 class="pro-price"><?php echo number_format($value->gia_tien);?>₫</h6>

                                                <div class="pro-icon">
                                                    <ul>
                                                        <!--                                                    <li class="heart-left"><a class="like" href="#"><i-->
                                                        <!--                                                                class="pe-7s-like"></i></a></li>-->
                                                        <li class="heart-left"><a href="#"><i
                                                                        class="pe-7s-cart"></i></a></li>
                                                        <li class="heart-left"><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='product_detail.php?id=<?php echo $value->id;?>'"><i class="pe-7s-search"></i></button></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--                                        <div class="single-product-bootom">-->
                                            <!--                                            <div class="pro-btm-text">-->
                                            <!--                                                <a href="#">Smoothie Foundation</a>-->
                                            <!--                                            </div>-->
                                            <!--                                            <div class="pro-star">-->
                                            <!--                                                        <span class="star"><i class="fa fa-star"-->
                                            <!--                                                                              aria-hidden="true"></i></span>-->
                                            <!--                                                <span class="star"><i class="fa fa-star"-->
                                            <!--                                                                      aria-hidden="true"></i></span>-->
                                            <!--                                                <span class="star"><i class="fa fa-star"-->
                                            <!--                                                                      aria-hidden="true"></i></span>-->
                                            <!--                                                <span class="star"><i class="fa fa-star"-->
                                            <!--                                                                      aria-hidden="true"></i></span>-->
                                            <!--                                                <span class="star-change"><i class="fa fa-star"-->
                                            <!--                                                                             aria-hidden="true"></i></span>-->
                                            <!--                                            </div>-->
                                            <!--                                        </div>-->
                                        </div>
                                        <!--End single-product -->
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <!-- End single middle product list -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End product list Area -->
<!-- start page navigation  -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-navigation">
                <ul class="navi">
                    <li><a href="#" class="double-angle"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li class="2"><a href="#">2</a></li>
                    <li class="3"><a href="#">3</a></li>
                    <li class="3"><a href="#">4</a></li>
                    <li class="3"><a href="#">...</a></li>
                    <li class="3"><a href="#">19</a></li>
                    <li><a href="#" class="double-angle"><i class="fa fa-angle-double-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<style>
    .banner-right-img img{height: 100px}
</style>
<!-- End page navigation  -->
<!-- Start footer Area -->