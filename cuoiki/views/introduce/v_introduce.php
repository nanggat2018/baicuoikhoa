<div class="sub-menu-area about-brth">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-menu">
                    <nav class="sub-menu-list">
                        <ul class="item">
                            <li><a href="#">Home<span class="shape-right"></span></a></li>
                            <li><a href="#">About Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End sub-menu Area -->
<!-- Start single blog area -->
<section class="history-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="history">
                    <h4>GIỚI THIỆU VỀ CHÚNG TÔI</h4>
                    <p class="about-para">“Hiểu người khác chính là chìa khóa để lãnh đạo thành công. Tại Leather điều quan trọng với chúng tôi là làm thế nào để mang lại trải nghiệm tốt nhất cho nhân viên,
                        đối tác và người dùng.“</p>
                    <p>Để định nghĩa chúng tôi là ai - thông qua lời nói hay cách ứng xử trong nhiều trường hợp khác nhau
                        - thì thực chất, chúng tôi gần gũi, vui vẻ và đồng lòng.
                        Đây là những đặc tính chính và nổi bật trong từng bước đường phát triển của Leather.</p>
                    <p>+ Chúng tôi tin tưởng vào sức mạnh khai triển của công nghệ và mong muốn góp phần làm cho thế giới
                        trở nên tốt đẹp hơn bằng việc kết nối cộng đồng người mua và người bán thông qua việc cung cấp
                        một nền tảng thương mại điện tử.</p>
                    <p>+ Đối với người dùng trên toàn khu vực, Leather mang đến trải nghiệm mua sắm trực tuyến tích hợp
                        với vô số sản phẩm đa dạng chủng loại, cộng đồng người dùng năng động và chuỗi dịch
                        vụ liền mạch.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about-right">
                    <img src="public/layout/assets/images/about%20us/1.webp" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End single blog area -->
<!-- Start team Area -->
<div class="team-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <!--start team content -->
                <div class="our-teams">
                    <h4>Mục tiêu của chúng tôi</h4>
                    <p> Là người đáng tin cậy, làm những gì chúng tôi nói rằng chúng tôi sẽ làm
                        nâng cao các tiêu chuẩn; không đi đường tắt, ngay cả khi không có ai đang giám sát
                        Hành động như một chủ sở hữu; chủ động tìm cách làm cho tổ chức của chúng tôi tốt hơn.</p>
                    <p class="team-style">“ Chủ động làm việc, không cần ai thúc đẩy
                        Luôn có tinh thần khẩn trương hoàn thành công việc. Khách hàng luôn đúng
                        vượt trên sự mong đợi của khách hàng, cung cấp vượt trội và hơn thế nữa“</p>
                    <p>"Dự đoán những thay đổi và lập kế hoạch trước
                        Chấp nhận những thay đổi không lường trước và cuối cùng đạt được mục tiêu
                        đã đặt ra.Hãy tin rằng chúng tôi luôn là kẻ yếu và tìm cách học hỏi từ thị trường
                        và các đối thủ cạnh tranh
                        Chấp nhận rằng chúng tôi không hoàn hảo, và sẽ không bao giờ
                        Làm việc chăm chỉ trước, ăn mừng và tận hưởng sau"</p>
                </div>
                <!--End team content -->
            </div>
            <div class="col-lg-6">
                <div class="row team-area-pro">
                    <div class="col-sm-4 col-6">
                        <!--start team profile -->
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/1.webp" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/2.webp" alt="" /></a>
                        </div>
                        <!--End team profile -->
                    </div>
                    <div class="col-sm-4 col-6">
                        <!--start team profile -->
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/3.webp" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/4.webp" alt="" /></a>
                        </div>
                        <!--End team profile -->
                    </div>
                    <div class="col-sm-4 col-6">
                        <!--start team profile -->
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/5.webp" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="team-pro">
                            <a href="#"><img src="public/layout/assets/images/blog/profile/6.webp" alt="" /></a>
                        </div>
                        <!--End team profile -->
                    </div>
                </div>
            </div>
        </div>\
    </div>
</div>