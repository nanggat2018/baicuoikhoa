<!-- Start slider Area -->
<section id="sasy-slider-area">
    <div class="sasy-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-area">
                        <div class="bend niceties preview-2">
                            <div id="ensign-nivoslider" class="slides">
                                <img src="admin/public/image_san_pham/nen1.png" alt="" title="#slider-direction-1" />
                                <img src="admin/public/image_san_pham/hover12.jpg" alt="" title="#slider-direction-2" />
                                <img src="admin/public/image_san_pham/nen3.jpg" alt="" title="#slider-direction-3" />
                            </div>
                            <!-- direction 1 -->
                            <div id="slider-direction-1" class="t-cn slider-direction">
                                <div class="slider-progress"></div>
                                <div class="slider-content t-cn s-tb slider-1">
                                    <div class="title-container s-tb-c title-compress">
                                        <h1 class="title1">Leather</h1>
                                        <h2 class="title2"><span>Love</span> Minimal Style</h2>
                                    </div>
                                </div>
                            </div>
                            <!-- direction 2 -->
                            <div id="slider-direction-2" class="t-cn slider-direction">
                                <div class="slider-progress"></div>
                                <div class="slider-content t-cn s-tb slider-1">
                                    <div class="title-container s-tb-c">
                                        <h1 class="title1">Leather</h1>
                                        <h2 class="title2"><span>Love</span> Minimal Style</h2>
                                    </div>
                                </div>
                            </div>
                            <!-- direction 3 -->
                            <div id="slider-direction-3" class="t-cn slider-direction">
                                <div class="slider-progress"></div>
                                <div class="slider-content t-cn s-tb slider-1">
                                    <div class="title-container s-tb-c">
                                        <h1 class="title1">Leather</h1>
                                        <h2 class="title2"><span>Love</span> Minimal Style</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End slider Area -->
<!-- Start banner Area -->
<div class="banner-areea">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="arival">
                    <h4 style="font-size: 15px">HÀNG MỚI VỀ</h4>
                </div>
                <div class="banner-s-item-owl-total owl-nav-syle-1">
                    <div class="banner-single-item-group">
                        <?php foreach ($new_products as $key=>$value) {?>
                        <div class="owl-left">
                            <div class="single-owl-item">
                                <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" /></a>
                                <div class="pro-price">
                                    <h2><?php echo number_format($value->gia_tien);?>₫</h2>
                                </div>
                                <div class="shape">
                                    <p><?php echo $value->ten_san_pham;?></p>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    </div>

                    <div class="banner-single-item-group">
                        <?php foreach ($new_products as $key=>$value) {?>
                        <div class="owl-left">
                            <div class="single-owl-item">
                                <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" /></a>
                                <div class="pro-price">
                                    <h2><?php echo number_format($value->gia_tien);?>₫</h2>
                                </div>
                                <div class="shape">
                                    <p><?php echo $value->ten_san_pham;?></p>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>

                </div>
            </div>



            <!-- Start banner Middle -->
            <div class="col-lg-6 col-md-6">
                <div class="banner-middle">
                    <div class="banner-content">
                        <a href="#"><img style="width: 75%;" src="admin/public/image_san_pham/b1.jpg" alt="" /></a>
                        <h2 style="color: #997171;">leather collection</h2>
                        <div class="banner-button">
                            <a href="#" class="btn-1">shop now</a>
                        </div>
                    </div>
                </div>
                <!-- Start banner Middle bootom -->
                <div class="row">
                    <div class="col-lg-6 bmbtml">
                        <div class="banner-btm-content">
                            <a href="#">
                                <img class="font-img" src="admin/public/image_san_pham/12.jpg" alt="" />
                                <img class="back-img" src="admin/public/image_san_pham/10.jpg" alt="" />
                                <p style="color: #997171;">bag collection</p>
                            </a>

                            <a href="#" class="btn-2">shop now</a>
                        </div>
                    </div>
                    <div class="col-lg-6 bmbtmr">
                        <div class="banner-btm-content">
                            <a href="#">
                                <img class="font-img" src="admin/public/image_san_pham/hover5.jpg" alt="" />
                                <img class="back-img" src="admin/public/image_san_pham/11.jpg" alt="" />
                                <p style="color: #997171;">shoulder bag collection</p>
                            </a>
                            <a href="#" class="btn-2">shop now</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End banner Middle -->


            <!-- Start banner Right -->
            <div class="col-lg-3 col-md-12 seller-mar">
                <div class="arival">
                    <h4 style="font-size: 15px">sản phẩm bán chạy</h4>
                </div>

                <!-- Single Right banner total owl -->
                <div class="banner-right-area owl-nav-syle-1">

                    <!-- Single Right banner group-->
                    <div class="banner-right-group">
                        <?php foreach ($wallet_products as $key=>$value) {?>
                        <div class="single-right-banner">
                            <div class="banner-right-img">
                                <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" /></a>
                            </div>
                            <div class="banner-right-content">

                                <a href="#"><?php echo $value->ten_san_pham; ?></a>
                                <h2><?php echo number_format($value->gia_tien)?>₫</h2>
                                <div class="pro-star banner">
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star-change"><i aria-hidden="true" class="fa fa-star"></i></span>
                                </div>
                            </div>
                        </div>
                        <!-- Single Right banner-->
                        <?php }?>
                    </div>

                    <!-- Single Right banner group-->
                    <div class="banner-right-group">
                        <?php foreach ($bag_products as $key=>$value) {?>
                        <div class="single-right-banner">
                            <div class="banner-right-img">
                                <a href="#"><img src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" /></a>
                            </div>
                            <div class="banner-right-content">
                                <a href="#"><?php echo $value->ten_san_pham; ?></a>
                                <h2><?php echo number_format($value->gia_tien)?>₫</h2>
                                <div class="pro-star banner">
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star"><i aria-hidden="true" class="fa fa-star"></i></span>
                                    <span class="star-change"><i aria-hidden="true" class="fa fa-star"></i></span>
                                </div>
                            </div>
                        </div>
                        <!-- Single Right banner-->
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- End banner Right -->
        </div>
    </div>
</div>
<!-- End banner Area -->

<section class="blog-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <!-- single blog  -->
                <div class="single-blog">
                    <a href="#"><img src="admin/public/image_san_pham/doihang.png" alt="" /></a>
                    <h6></h6>
                    <a class="s-blog-head" href="#">Đổi  hàng trong 10 ngày</a>
                    <p>Khách hàng vui lòng mang sản phẩm tới cửa hàng.Nhân viên bảo hành sẽ kiểm tra,đánh giá,
                        xác định nguyên nhân gây lỗi.</p>
                    <a href="#" class="read-more">read more</a>
                </div>
                <!-- single blog  -->
            </div>
            <div class="col-md-4">
                <!-- single blog  -->
                <div class="single-blog">
                    <a href="#"><img src="admin/public/image_san_pham/baohanh.png" alt="" /></a>
                    <h6></h6>
                    <a class="s-blog-head" href="#"> Bảo hành </a>
                    <p>Bảo trì sản phẩm trọn đời với quá trình làm mới, làm sạch , làm bóng, làm mềm và lỗi có
                        thể khắc phục.</p>
                    <a href="#" class="read-more">read more</a>
                </div>
            </div>
            <div class="col-md-4">
                <!-- single blog  -->
                <div class="single-blog">
                    <a href="#"><img src="admin/public/image_san_pham/giaonhanh.png" alt="" /></a>
                    <h6></h6>
                    <a class="s-blog-head" href="#">Giao hàng nhanh</a>
                    <p>Khách hàng không còn phải lo ngại về mặt thời gian, hàng sẽ được giao
                        đến tay khách hàng nhanh chóng.</p>
                    <a href="#" class="read-more">read more</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End blog area -->



<!-- Start Products Area -->
<section id="product-area" class="rjc">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-heading-area">
                    <div class="product-heading">
                        <h2 style="font-size: 15px">Sản phẩm tiêu biểu</h2>
                    </div>
                    <div class="tab-product-menu">
<!--                        Nav tabs-->
                        <ul class="nav pro-tab-menu">
                            <li><a class="active" href="#all" data-bs-toggle="tab">Túi sách</a></li>
                            <li><a href="#women" data-bs-toggle="tab">Ví</a></li>
                            <li><a href="#men" data-bs-toggle="tab">Túi du lịch</a></li>
                            <li><a href="#kid" data-bs-toggle="tab">Túi đeo vai</a></li>
                        </ul>
                    </div>
                </div
                <!-- product content -->
                <div class="product-content">
                    <div class="tab-content">

                        <!--Start product All -->
                        <div role="tabpanel" class="tab-pane fade show active" id="all">

                            <div class="product-group">
                                <div class="row">
                                    <?php foreach ($bag_products as $key=>$value){ ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <!--start project Tui Sach -->
                                        <div class="single-product">
                                            <div class="single-product-top">
                                                <a href="#">
                                                    <img class="pro-font-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh ?>" alt="" />
                                                    <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh_hover ?>" alt="" />
                                                </a>
                                                <h3 class="pro-price"><?php echo number_format($value->gia_tien)?>₫</h3>
                                                <div class="pro-icon">
                                                    <ul>
                                                        <li class="heart-left"><a class="like" href="#"><i class="pe-7s-like"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-product-bootom">
                                                <div class="pro-btm-text">
                                                    <a href="#"><?php echo $value->ten_san_pham ?></a>
                                                </div>
                                                <div class="pro-star">
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star-change"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End single-product -->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                        <!--End product Tui Sach -->

                        <!--Start product Ví -->
                        <div role="tabpanel" class="tab-pane fade" id="women">
                            <div class="product-group">
                                <div class="row">
                                    <?php foreach ($wallet_products as $key=>$value){ ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <!--start single-project -->
                                        <div class="single-product">
                                            <div class="single-product-top">
                                                <a href="#">
                                                    <img class="pro-font-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh;?>" alt="" />
                                                    <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh_hover;?>" alt="" />
                                                </a>
                                                <h3 class="pro-price"><?php echo number_format($value->gia_tien)?>₫</h3>
                                                <div class="pro-icon">
                                                    <ul>
                                                        <li class="heart-left"><a class="like" href="#"><i class="pe-7s-like"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-product-bootom">
                                                <div class="pro-btm-text">
                                                    <a href="#"><?php echo $value->ten_san_pham ?></a>
                                                </div>
                                                <div class="pro-star">
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star-change"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End single-product -->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--End product Ví-->

                        <!--Start product Túi du lịch -->
                        <div role="tabpanel" class="tab-pane fade" id="men">
                            <div class="product-group">
                                <div class="row">
                                    <?php foreach ($travel_products as $key=>$value){ ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <!--start single-project -->
                                        <div class="single-product">
                                            <div class="single-product-top">
                                                <a href="#">
                                                    <img class="pro-font-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh; ?>" alt="" />
                                                    <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh_hover; ?>" alt="" />
                                                </a>
                                                <h3 class="pro-price"><?php echo number_format($value->gia_tien)?>₫</h3>
                                                <div class="pro-icon">
                                                    <ul>
                                                        <li class="heart-left"><a class="like" href="#"><i class="pe-7s-like"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-product-bootom">
                                                <div class="pro-btm-text">
                                                    <a href="#"><?php echo $value->ten_san_pham;?></a>
                                                </div>
                                                <div class="pro-star">
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star-change"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End single-product -->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--End product Túi du lịch -->

                        <!--Start product Túi đeo vai <balo> -->
                        <div role="tabpanel" class="tab-pane fade" id="kid">
                            <div class="product-group">
                                <div class="row">
                                    <?php foreach ($shoulder_products as $key=>$value){ ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6">
                                        <!--start single-project -->
                                        <div class="single-product">
                                            <div class="single-product-top">
                                                <a href="#">
                                                    <img class="pro-font-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh; ?>" alt="" />
                                                    <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $value->hinh_anh; ?>" alt="" />
                                                </a>
                                                <h3 class="pro-price"><?php echo number_format($value->gia_tien)?>₫</h3>
                                                <div class="pro-icon">
                                                    <ul>
                                                        <li class="heart-left"><a class="like" href="#"><i class="pe-7s-like"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a></li>
                                                        <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-product-bootom">
                                                <div class="pro-btm-text">
                                                    <a href="#"<?php echo $value->ten_san_pham ?>></a>
                                                </div>
                                                <div class="pro-star">
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                    <span class="star-change"><i class="fa fa-star" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--End Túi đeo vai balo-product -->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--End product Kid -->

                    </div>
                    <div class="loadmore">
                        <span class="current 0">Load more</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .slides img{height: 500px;}
    .owl-left {height: 270px;width: 250px;}
    .banner-right-img img{max-width: 100%;width: 110px;}
    .single-blog img{width: 70px;}
    .single-blog{text-align: center;margin-top: 40px;}
    a.read-more {margin-left: 125px;}
</style>
<!-- End Products Area -->

<!-- Start footer Area -->