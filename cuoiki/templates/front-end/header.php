<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Start header Area -->
<div class="wraper">
    <header id="header" class="header-area">
        <div id="header-area">
            <!-- header top Start -->
            <div class="header-top">
                <div class="container">

                    <div class="row align-items-center">

                        <!-- Start language -->
                        <div class="col-md-4 col-4 order-2 order-md-1">
                            <div class="language">
                                <ul>
                                    <li><a href="#">eng</a></li>
                                    <li><a href="#">usd</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End language -->

                        <!-- Start logo -->
                        <div class="col-md-4 order-1 order-md-2">
                            <div class="logo tnm text-center">
                                <a href="#"><img style="width: 180px" src="admin/public/image_san_pham/logo.png"></a>
                            </div>
                        </div>
                        <!-- End logo -->

                        <!-- Start cart -->
                        <div class="col-md-4 col-8 order-3 order-md-3">
                            <div class="cart-actions">
                                <a class="cart-action" href="#">
                                    <p><i class="pe-7s-unlock"></i> Tài khoản</p>
                                </a>
                                <div class="dropdown">
                                    <button class="cart-action" data-bs-toggle="dropdown">
                                        <p><i class="pe-7s-cart"></i> Giỏ hàng</p>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end cart-dropdown">
                                        <div class="cart-dropdown-itme">
                                            <a class="cart-img" href="shopping-cart.html">
                                                <img src="public/layout/assets/images/cart/1.webp" alt="cart"/>
                                            </a>
                                            <div class="menu-cart-text">
                                                <h5 class="title"><a href="#">men’s black t-shirt</a></h5>
                                                <span>Color : Black</span>
                                                <strong>$122.00</strong>
                                            </div>
                                            <button class="remove"><i class="pe-7s-close-circle"></i></button>
                                        </div>
                                        <div class="cart-dropdown-itme">
                                            <a class="cart-img" href="shopping-cart.html">
                                                <img src="public/layout/assets/images/cart/2.webp" alt="cart"/>
                                            </a>
                                            <div class="menu-cart-text">
                                                <h5 class="title"><a href="#">men’s black t-shirt</a></h5>
                                                <span>Color : Black</span>
                                                <strong>$122.00</strong>
                                            </div>
                                            <button class="remove"><i class="pe-7s-close-circle"></i></button>
                                        </div>

                                        <div class="total">
                                            <p>total <strong>= $306.00</strong></p>
                                        </div>
                                        <div class="cart-btn">
                                            <a class="btn goto" href="#">go to cart</a>
                                            <a class="btn out-menu" href="#">Check out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End cart -->
                    </div>

                </div>
            </div>
            <!-- header top End -->

            <!-- Start main Menu Area -->
            <div class="main-menu-areea">
                <div class="container">
                    <div class="main-menu-wrapper">
                        <div class="main-menu">
                            <nav class="manu-list">
                                <ul>
                                    <li><a href="index.php">trang chủ</a>
                                        <!-- dropdown -->
<!--                                        <ul class="dropdown-main">-->
<!--                                            <li><a href="index.html">Home Verision 1</a></li>-->
<!--                                            <li><a href="index-2.html">Home Verision 2</a></li>-->
<!--                                            <li><a href="index-3.html">Home Verision 3</a></li>-->
<!--                                            <li><a href="index-4.html">Home Verision 4</a></li>-->
<!--                                            <li><a href="index-5.html">Home Verision 5</a></li>-->
<!--                                            <li><a href="index-6.html">Home Verision 6</a></li>-->
<!--                                        </ul>-->
                                    </li>
                                    <!-- Megamenu -->
                                    <li class="plus sen-mega-active"><a href="product_grid.php">sản phẩm</a>
<!--                                        <div class="megamenu-area">-->
<!--                                            <ul class="megamenu">-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li class="mega-title"><a href="#">Dresses</a></li>-->
<!--                                                        <li><a href="#">Sweater</a></li>-->
<!--                                                        <li><a href="#">Evening</a></li>-->
<!--                                                        <li><a href="#">Day</a></li>-->
<!--                                                        <li><a href="#">Sports</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li class="mega-title"><a href="#">Shoes</a></li>-->
<!--                                                        <li><a href="#">Ankle Boots</a></li>-->
<!--                                                        <li><a href="#">Clog sandals </a></li>-->
<!--                                                        <li><a href="#">Run</a></li>-->
<!--                                                        <li><a href="#">Books</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li class="mega-title"><a href="#">Hand Bags</a></li>-->
<!--                                                        <li><a href="#">Shoulder</a></li>-->
<!--                                                        <li><a href="#">Satchels</a></li>-->
<!--                                                        <li><a href="#">Kids</a></li>-->
<!--                                                        <li><a href="#">Coats</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li class="mega-title"><a href="#">Clothing</a></li>-->
<!--                                                        <li><a href="#">Coats Jackets</a></li>-->
<!--                                                        <li><a href="#">Raincoats</a></li>-->
<!--                                                        <li><a href="#">Jackets</a></li>-->
<!--                                                        <li><a href="#">T-shirts</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                            </ul>-->
<!--                                        </div>-->
                                    </li>
                                    <li><a  href="introduce.php">giới thiệu</a></li>
                                    <li><a href="#">thanh toán</a></li>
                                    <li><a href="#">đơn hàng</a></li>
                                    <li><a href="#">liên hệ</a></li>
<!--                                    <li><a href="#">pages</a>-->
<!--                                        <div class="megamenu-area">-->
<!--                                            <ul class="megamenu pages">-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li><a href="about-us.html">About us</a></li>-->
<!--                                                        <li><a href="blog.html">Blog</a></li>-->
<!--                                                        <li><a href="blog-single.html">Single blog</a></li>-->
<!--                                                        <li><a href="checkout.html">Checkout</a></li>-->
<!--                                                        <li><a href="contact.html">Contact</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                                <li class="single-item">-->
<!--                                                    <ul class="megamulist">-->
<!--                                                        <li><a href="product-detail.html">Product detail</a></li>-->
<!--                                                        <li><a href="product-list.html">Product list </a></li>-->
<!--                                                        <li><a href="products-grid.html">Products grid</a></li>-->
<!--                                                        <li><a href="shopping-cart.html">Shopping cart</a></li>-->
<!--                                                        <li><a href="wish-list.html">Wish list</a></li>-->
<!--                                                    </ul>-->
<!--                                                </li>-->
<!--                                            </ul>-->
<!--                                        </div>-->
<!--                                    </li>-->
<!--                                    <li><a class="no-margin" href="contact.html">contact</a></li>-->
                                </ul>
                            </nav>
                        </div>
                        <!-- End main Menu Area -->

                        <!-- Start search -->
                        <ul class="top-action-area d-none d-lg-block">
                            <li><a href="#"><i class="pe-7s-search"></i></a>
                                <form class="top-search-form" action="#">
                                    <input type="text" placeholder="Search your key...">
                                    <button type="submit"><i class="pe-7s-search"></i></button>
                                </form>
                            </li>
                        </ul>
                        <!-- End search -->
                    </div>
                </div>
            </div>
            <!-- Start main Menu End -->

            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="menu">
                                        <li><a href="index.html">Home</a>
                                            <ul>
                                                <li><a href="index.html">Home Verision 1</a></li>
                                                <li><a href="index-2.html">Home Verision 2</a></li>
                                                <li><a href="index-3.html">Home Verision 3</a></li>
                                                <li><a href="index-4.html">Home Verision 4</a></li>
                                                <li><a href="index-5.html">Home Verision 5</a></li>
                                                <li><a href="index-6.html">Home Verision 6</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="products-grid.html">sản phẩm</a>
                                            <ul>
                                                <li><a href="#">Dresses</a>
                                                    <ul>
                                                        <li><a href="#">Sweater</a></li>
                                                        <li><a href="#">Evening</a></li>
                                                        <li><a href="#">Day</a></li>
                                                        <li><a href="#">Sports</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Shoes</a>
                                                    <ul>
                                                        <li><a href="#">Ankle Boots</a></li>
                                                        <li><a href="#">Clog sandals </a></li>
                                                        <li><a href="#">Run</a></li>
                                                        <li><a href="#">Books</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">HandBags</a>
                                                    <ul>
                                                        <li><a href="#">Shoulder</a></li>
                                                        <li><a href="#">Satchels</a></li>
                                                        <li><a href="#">Kids</a></li>
                                                        <li><a href="#">Coats</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Clothing</a>
                                                    <ul>
                                                        <li><a href="#">Coats Jackets</a></li>
                                                        <li><a href="#">Raincoats</a></li>
                                                        <li><a href="#">Jackets</a></li>
                                                        <li><a href="#">T-shirts</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#">giới thiệu</a></li>
                                        <li><a href="#">thanh toán</a></li>
                                        <li><a href="#">đơn hàng</a></li>
                                        <li><a href="blog.html">blog</a></li>
                                        <li><a class="no-margin" href="#">Pages</a>
                                            <ul>
                                                <li><a href="about-us.html">About us</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="blog-single.html">Single blog</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                                <li><a href="contact.html">Contact</a></li>
                                                <li><a href="product-detail.html">Product detail</a></li>
                                                <li><a href="product-list.html">Product list </a></li>
                                                <li><a href="products-grid.html">Products grid</a></li>
                                                <li><a href="shopping-cart.html">Shopping cart</a></li>
                                                <li><a href="wish-list.html">Wish list</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="no-margin" href="contact.html">contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end -->
        </div>
    </header>
    <!-- End header Area -->