
<div class="page-wrapper">

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                    <div class="card-body">
                        <h4 class="card-title">Thêm quyền</h4>
                        <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên quyền</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_quyen" name="ten_quyen" placeholder="Tên quyền">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option>---Chọn---</option>
                                        <option value="1">Mở</option>
                                        <option value="0">Khóa</option>
                                     </select>
                                </div>

                            </div>
                        </div>
                    <div class="border-top">
                        <div class="card-body">

<!--                            <form  action="../c_quyen.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary" >Submit</button>

<!--                            </form>-->

<!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- editor -->

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
