<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Người quản trị</h5>
                        <div class="table-responsive">
                            <button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='add_nguoi_quan_tri.php'">Add</button>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Tên người quản trị</th>
                                    <th>Trạng thái</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Mật khẩu</th>
                                    <th>Địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Ngày sinh</th>
                                    <th>Id quyền</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($nguoi_quan_tri as $key=>$value){
                                    $css_trang_thai = $value->trang_thai ? "badge-info": "badge-danger";
                                    $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";
                                    ?>

                                    <tr>
                                        <td><?php echo $value->ten_nguoi_quan_tri; ?></td>
                                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>
                                        <td><?php echo $value->ten_dang_nhap; ?></td>
                                        <td><?php echo $value->mat_khau; ?></td>
                                        <td><?php echo $value->dia_chi; ?></td>
                                        <td><?php echo $value->so_dien_thoai; ?></td>
                                        <td><?php echo $value->email; ?></td>
                                        <td><?php echo $value->ngay_sinh; ?></td>
                                        <td><?php echo $value->id_quyen; ?></td>

                                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_nguoi_quan_tri.php?id=<?php echo $value->id;?>'">Edit</button>
                                            <button type="button" name="delete" class="btn btn-danger btn-sm" style="margin-top: 10px" onclick="window.location.href='delete_nguoi_quan_tri.php?id=<?php echo $value->id;?>' " >Delete</button></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>