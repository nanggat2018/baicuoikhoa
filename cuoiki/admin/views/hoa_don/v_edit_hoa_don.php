
<div class="page-wrapper">

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Sửa hóa đơn</h4>

                            <div class="form-group row" >
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <?php
                                    $text_trang_thai = $hoa_don_detail->trang_thai ? "Mở" : "Đóng"; ?>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option value="<?php echo $hoa_don_detail->trang_thai;?>"><?php echo $text_trang_thai;?></option>
                                        <option value="1">Mở</option>
                                        <option value="0">Đóng</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Số lượng</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="so_luong" name="so_luong" value="<?php echo $hoa_don_detail->so_luong;?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Id khách hàng</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="id_khach_hang" name="id_khach_hang" value="<?php echo $hoa_don_detail->id_khach_hang;?>">
                                </div>
                            </div>



                        </div>
                        <div class="border-top">
                            <div class="card-body">

                                <!--                            <form  action="../c_banner.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Submit</button>

                                <!--                            </form>-->

                                <!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
</div>
