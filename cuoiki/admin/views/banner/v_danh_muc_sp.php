
<div class="page-wrapper">

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                    <div class="card-body">
                        <h4 class="card-title">Thêm danh mục sản phẩm</h4>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên danh mục sản phẩm</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="ten_danh_muc" name="ten_danh_muc" placeholder="Tên danh mục sản phẩm">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                            <div class="col-sm-9">
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                    <option>---Chọn---</option>
                                    <option value="1">Mở</option>
                                    <option value="2">Đóng</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên người đăng</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="ten_nguoi_dang" name="nguoi_dang" placeholder="Tên người đăng">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Thời gian đăng</label>
                            <div class="col-sm-9">
                                <input type="datetime-local" class="form-control" id="thoi_gian_dang" name="thoi_gian_dang" placeholder="Thời gian đăng">
                            </div>
                        </div>

<!--                        <div class="form-group row">-->
<!--                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên người chỉnh sửa</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="text" class="form-control" id="ten_nguoi_chinh_sua" name="nguoi_chinh_sua" placeholder="Tên người chỉnh sửa">-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="form-group row">-->
<!--                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Thời gian chỉnh sửa</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="datetime-local" class="form-control" id="thoi_gian_chinh_sua" name="thoi_gian_chinh_sua" placeholder="Thời gian chỉnh sửa">-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--
<!---->
<!--                        <div class="form-group row">-->
<!--                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Hình ảnh</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <div class="custom-file">-->-->
<!--                                <input type="file" name="f_hinh_anh" class="custom-file-input" id="validatedCustomFile" required>-->
<!--                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>-->
<!--                                <div class="invalid-feedback">Example invalid custom file feedback</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        </div>-->


                    </div>
                    <div class="border-top">
                        <div class="card-body">

<!--                            <form  action="../c_banner.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Submit</button>

<!--                            </form>-->

<!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- editor -->

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
