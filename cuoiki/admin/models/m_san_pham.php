
<?php
require_once ("database.php");
class m_san_pham extends database{
    public function insert_san_pham($id,$ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham){
            $sql = "insert into san_pham value (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $this->setQuery($sql);
            return $this->execute(array($id,$ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham));
    }


    public function read_san_pham(){
        $sql = "select * from san_pham";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_san_pham_by_id_san_pham($id){
        $sql = "select * from san_pham where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_san_pham($id,$ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham){
        $sql = "update san_pham set ten_san_pham = ?,trang_thai = ?,mo_ta_chi_tiet = ?,gia_tien = ?,so_luong = ?,hinh_anh = ?,hinh_anh_hover = ?,nguoi_dang = ?,thoi_gian_dang = ?,nguoi_chinh_sua = ?,thoi_gian_sua = ?,id_danh_muc_san_pham = ? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham,$id));
    }
    public function delete_san_pham($id){

        $sql = "delete from san_pham where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}

