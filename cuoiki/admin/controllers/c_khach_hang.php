<?php
include ("models/m_khach_hang.php");
class c_khach_hang
{
    public function show_khach_hang()
    {
        $m_khach_hang = new m_khach_hang();
        $khach_hang = $m_khach_hang->read_khach_hang();

        $view = "views/khach_hang/v_khach_hang.php";
        include("templates/layout.php");

    }

    public function edit_khach_hang()
    {
        $m_khach_hang = new m_khach_hang();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_khach_hang->read_khach_hang_by_id_khach_hang($id);

            $khach_hang_detail = $m_khach_hang->read_khach_hang_by_id_khach_hang($id);

            if (isset($_POST['btn_submit'])) {
                $ten_khach_hang = $_POST['ten_khach_hang'];
                $trang_thai = $_POST['trang_thai'];
                $ngay_sinh = $_POST['ngay_sinh'];
                $dia_chi = $_POST['dia_chi'];
                $so_dien_thoai = $_POST['so_dien_thoai'];
                $email = $_POST['email'];

//                $so_luong = $_POST['so_luong'];
//                $id_khach_hang= $_POST['id_khach_hang'];

                $result = $m_khach_hang->edit_khach_hang($id,$ten_khach_hang,$trang_thai,$ngay_sinh, $dia_chi, $so_dien_thoai, $email);
//                if ($result) {
//                    if ($hinh_tieu_de != "") {
//                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "public/image_san_pham/" . $hinh_tieu_de);
//                    }
//                    echo "<script>alert('thành công')</script>";
//                } else {
//                    echo "<script>alert('không thành công')</script>";
//
//                }

            }

        }
        $view = "views/khach_hang/v_edit_khach_hang.php";
        include("templates/layout.php");
    }

    function delete_khach_hang()
    {
        if(isset($_GET["id"]))
        {
            $id= $_GET["id"];
            $m_khach_hang= new m_khach_hang();
            $kq = $m_khach_hang->delete_khach_hang($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='khach_hang.php'</script>";

            }
        }
    }

}

?>



