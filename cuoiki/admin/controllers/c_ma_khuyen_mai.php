<?php
include ("models/m_ma_khuyen_mai.php");
class c_ma_khuyen_mai
{
    public function show_ma_khuyen_mai()
    {
        $m_ma_khuyen_mai = new m_ma_khuyen_mai();
        $ma_khuyen_mai = $m_ma_khuyen_mai->read_ma_khuyen_mai();

        $view = "views/ma_khuyen_mai/v_ma_khuyen_mai.php";
        include("templates/layout.php");

    }

    public function add_ma_khuyen_mai(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $ten_ma_khuyen_mai= $_POST['ten_ma_khuyen_mai'];
            $trang_thai = $_POST['trang_thai'];
            $nguoi_dang = $_POST['nguoi_dang'];
            $thoi_gian_dang = $_POST['thoi_gian_dang'];
            $nguoi_chinh_sua = NUll;
            $thoi_gian_sua = NULL;
            $id_san_pham = $_POST['id_san_pham'];


            $ma_khuyen_mai = new m_ma_khuyen_mai();
            $result = $ma_khuyen_mai->insert_ma_khuyen_mai($id,$ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham);
            if($result)
            {
                echo "<script>alert('Thêm thành công');window.location='ma_khuyen_mai.php'</script>";

            }
//
        }

        $view = "views/ma_khuyen_mai/v_add_ma_khuyen_mai.php";
        include ("templates/layout.php");
    }

    public function edit_ma_khuyen_mai()
    {
        $m_ma_khuyen_mai = new m_ma_khuyen_mai();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_ma_khuyen_mai->read_ma_khuyen_mai_by_id_ma_khuyen_mai($id);
            $ma_khuyen_mai_detail = $m_ma_khuyen_mai->read_ma_khuyen_mai_by_id_ma_khuyen_mai($id);

            if (isset($_POST['btn_submit'])) {
                $ten_ma_khuyen_mai = $_POST['ten_ma_khuyen_mai'];
                $trang_thai = $_POST['trang_thai'];
                $nguoi_dang = $_POST['nguoi_dang'];
                $thoi_gian_dang = $_POST['thoi_gian_dang'];
                $nguoi_chinh_sua = $_POST['nguoi_chinh_sua'];
                $thoi_gian_sua = $_POST['thoi_gian_sua'];
                $id_san_pham = $_POST['id_san_pham'];
//
                $result = $m_ma_khuyen_mai->edit_ma_khuyen_mai($id,$ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham);
//                if ($result) {
//                    if ($hinh_tieu_de != "") {
//                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "public/image_san_pham/" . $hinh_tieu_de);
//                    }
//                    echo "<script>alert('thành công')</script>";
//                } else {
//                    echo "<script>alert('không thành công')</script>";
//
//                }

            }

        }
        $view = "views/ma_khuyen_mai/v_edit_ma_khuyen_mai.php";
        include("templates/layout.php");
    }

    function delete_ma_khuyen_mai()
    {
        if(isset($_GET["id"]))
        {
            $id= $_GET["id"];
            $m_ma_khuyen_mai= new m_ma_khuyen_mai();
            $kq = $m_ma_khuyen_mai->delete_ma_khuyen_mai($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='ma_khuyen_mai.php'</script>";

            }
        }
    }


}

?>



