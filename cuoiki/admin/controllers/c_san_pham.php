<?php
include ("models/m_san_pham.php");
class c_san_pham{
    public function show_san_pham()
    {
        $m_san_pham = new m_san_pham();
        $san_phams = $m_san_pham->read_san_pham();

        $view = "views/san_pham/v_san_pham.php";
        include("templates/layout.php");

    }
    public function add_san_pham(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $ten_san_pham = $_POST['ten_san_pham'];
            $trang_thai = $_POST['trang_thai'];
            $mo_ta_chi_tiet = $_POST['mo_ta_chi_tiet'];
            $gia_tien = $_POST['gia_tien'];
            $so_luong = $_POST['so_luong'];
            $nguoi_dang = $_POST['nguoi_dang'];
            $thoi_gian_dang = $_POST['thoi_gian_dang'];
            $nguoi_chinh_sua = NULL;
            $thoi_gian_sua = NULL;
            $id_danh_muc_san_pham = $_POST['danh_muc_san_pham'];
            // lấy đc tên hình ảnh
            $hinh_anh = ($_FILES['hinh_anh']['error']==0)? $_FILES['hinh_anh']['name']:"";
            $hinh_anh_hover = ($_FILES['hinh_anh_hover']['error']==0)? $_FILES['hinh_anh_hover']['name']:"";

            $san_pham = new m_san_pham();
            $result = $san_pham->insert_san_pham($id,$ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham);

             if ($result) {
                    if ($hinh_anh != "" && $hinh_anh_hover != "") {
                        move_uploaded_file($_FILES['hinh_anh']['tmp_name'], "public/image_san_pham/".$hinh_anh);
                        move_uploaded_file($_FILES['hinh_anh_hover']['tmp_name'], "public/image_san_pham/".$hinh_anh_hover);

                    }
                    echo "<script>alert('thành công')</script>";
                } else {
                    echo "<script>alert('không thành công')</script>";

                }

//            if($result) {
////                if ($hinh_anh != "" && $hinh_anh_hover != "") {
//                if ($hinh_anh != "") {
//
//                    move_uploaded_file($_FILES['hinh_anh']['tmp_name'], "public/imagesan_pham/".$hinh_anh);
////                   and move_uploaded_file($_FILES['hinh_anh_hover']['tmp_name'], "public/imagesan_pham/".$hinh_anh_hover);
//                }
//                echo "<script>alert('Thành công')</script>";
//            }else{
//                echo "<script>alert('Không thành công')</script>";
//
//                }
            }



        $view = "views/san_pham/v_add_san_pham.php";
        include ("templates/layout.php");
    }
    public function edit_san_pham()
    {
        $m_san_pham = new m_san_pham();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $san_pham_detail = $m_san_pham->read_san_pham_by_id_san_pham($id);
            if (isset($_POST['btn-submit'])) {
                $ten_san_pham = $_POST['ten_san_pham'];
                $trang_thai = $_POST['trang_thai'];
                $mo_ta_chi_tiet = $_POST['mo_ta_chi_tiet'];
                $gia_tien = $_POST['gia_tien'];
                $so_luong = $_POST['so_luong'];
                $nguoi_dang = $_POST['nguoi_dang'];
                $thoi_gian_dang = $_POST['thoi_gian_dang'];
                $nguoi_chinh_sua = $_POST['nguoi_chinh_sua'];
                $thoi_gian_sua = $_POST['thoi_gian_sua'];
                $id_danh_muc_san_pham = $_POST['danh_muc_san_pham'];
                // lấy đc tên hình ảnh

                $hinh_anh = ($_FILES['hinh_anh']['error']==0)? $_FILES['hinh_anh']['name']:"";
                $hinh_anh_hover = ($_FILES['hinh_anh_hover']['error']==0)? $_FILES['hinh_anh_hover']['name']:"";

                $san_pham = new m_san_pham();
                $result = $san_pham->edit_san_pham($id,$ten_san_pham,$trang_thai,$mo_ta_chi_tiet,$gia_tien,$so_luong,$hinh_anh,$hinh_anh_hover,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_danh_muc_san_pham);
                if ($result) {
                    if ($hinh_anh != "" && $hinh_anh_hover != "") {
                        move_uploaded_file($_FILES['hinh_anh']['tmp_name'], "public/image_san_pham/".$hinh_anh);
                        move_uploaded_file($_FILES['hinh_anh_hover']['tmp_name'], "public/image_san_pham/".$hinh_anh_hover);

                    }
                    echo "<script>alert('thành công')</script>";
                } else {
                    echo "<script>alert('không thành công')</script>";

                }
                }
        }
            $view = "views/san_pham/v_edit_san_pham.php";
            include("templates/layout.php");
        }

    function delete_san_pham()
    {
        if(isset($_GET["id"]))
        {
            $id= $_GET["id"];
            $m_san_pham= new m_san_pham();
            $kq = $m_san_pham->delete_san_pham($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='san_pham.php'</script>";

            }
        }
    }
}
?>



