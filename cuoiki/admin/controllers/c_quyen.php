<?php
include ("models/m_quyen.php");
class c_quyen{
    public function show_quyen()
    {
        $m_quyen = new m_quyen();
        $quyens = $m_quyen->read_quyen();

        $view = "views/quyen/v_quyen.php";
        include("templates/layout.php");

    }
    public function add_quyen(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $ten_quyen = $_POST['ten_quyen'];
            $trang_thai = $_POST['trang_thai'];

            $quyen = new m_quyen();
            $result = $quyen->insert_quyen($id,$ten_quyen,$trang_thai);
            if($result)
            {
                echo "<script>alert('Thêm thành công');window.location='quyen.php'</script>";

            }
            }

        $view = "views/quyen/v_add_quyen.php";
        include ("templates/layout.php");
    }
    public function edit_quyen()
    {
        $m_quyen = new m_quyen();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $quyen_detail = $m_quyen->read_quyen_by_id_quyen($id);
            if (isset($_POST['btn-submit'])) {
                // $id = NULL;
                $ten_quyen = $_POST['ten_quyen'];
                $trang_thai = $_POST['trang_thai'];

                $quyen = new m_quyen();
                $result = $quyen->edit_quyen($id,$ten_quyen,$trang_thai);
                if($result)
                {
                    echo "<script>alert('Sửa thành công');window.location='quyen.php'</script>";

                }

                }
            }
            $view = "views/quyen/v_edit_quyen.php";
            include("templates/layout.php");
    }
    function  delete_quyen()
    {
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
            $m_quyen = new m_quyen();
            $kq = $m_quyen->delete_quyen($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='quyen.php'</script>";
                
            }
        }
    }  
}
?>