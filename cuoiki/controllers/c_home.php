<?php
include ("models/m_product.php");
class c_home{
    public function __construct()
    {
    }
    public function index(){
        $m_product = new m_product();

        $new_products =  $m_product->new_product();

        $wallet_products =  $m_product->wallet_product();

        $bag_products = $m_product->bag_product();

        $travel_products = $m_product->travel_product();

        $shoulder_products = $m_product->shoulder_product();

        $typical_products = $m_product->typical_product();

        $view = "views/home/v_home.php";
        include ("templates/front-end/layout.php");
    }

}
?>



