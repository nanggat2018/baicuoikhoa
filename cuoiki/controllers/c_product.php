<?php
include ("models/m_product.php");
class c_product{
    public function __construct()
    {
    }
    public function product_grid(){
        $m_product = new m_product();

        $products =  $m_product->read_product();
        $bag_products = $m_product->bag_product();
        $wallet_products =  $m_product->wallet_product();
        $product_category = $m_product->read_product_category();


        $view = "views/product/v_product_grid.php";
        include ("templates/front-end/layout.php");

    }

    public function product_detail(){

        $m_product = new m_product();
//        $products =  $m_product->read_product_by_id_product();

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $product_detail = $m_product->read_product_by_id_product($id);
            }

        $view = "views/product/v_product_detail.php";
        include ("templates/front-end/layout.php");

    }

    public function product_category(){
        $m_product = new m_product();

        $bag_products = $m_product->bag_product();
        $wallet_products =  $m_product->wallet_product();

        $product_category = $m_product->read_product_category();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $product_category_detail = $m_product->read_product_category_by_id($id);
        }

        $view = "views/product/v_product_category.php";
        include ("templates/front-end/layout.php");




    }



}
?>
